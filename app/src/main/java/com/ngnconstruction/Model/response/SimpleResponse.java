package com.ngnconstruction.Model.response;

import com.google.gson.annotations.SerializedName;

public class SimpleResponse  {
    @SerializedName("status")
    private String status;

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
