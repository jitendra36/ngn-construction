package com.ngnconstruction.Model.response;

import java.util.ArrayList;

public class JobTimingResponse {
    private String code;

    private Data data;

    private String message;

    private String status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ClassPojo [code = " + code + ", data = " + data + ", message = " + message + ", status = " + status + "]";
    }

    public class Data {
        private String total_hours;

        private ArrayList<Hoursdata> hoursdata;

        public String getTotal_hours() {
            return total_hours;
        }

        public void setTotal_hours(String total_hours) {
            this.total_hours = total_hours;
        }

        public ArrayList<Hoursdata> getHoursdata() {
            return hoursdata;
        }

        public void setHoursdata(ArrayList<Hoursdata> hoursdata) {
            this.hoursdata = hoursdata;
        }

        @Override
        public String toString() {
            return "ClassPojo [total_hours = " + total_hours + ", hoursdata = " + hoursdata + "]";
        }
    }

    public class Hoursdata {
        private String hours;

        private String start_datetime;

        private String end_datetime;

        private String job_title;

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getStart_datetime() {
            return start_datetime;
        }

        public void setStart_datetime(String start_datetime) {
            this.start_datetime = start_datetime;
        }

        public String getEnd_datetime() {
            return end_datetime;
        }

        public void setEnd_datetime(String end_datetime) {
            this.end_datetime = end_datetime;
        }

        public String getJob_title() {
            return job_title;
        }

        public void setJob_title(String job_title) {
            this.job_title = job_title;
        }

        @Override
        public String toString() {
            return "ClassPojo [hours = " + hours + ", start_datetime = " + start_datetime + ", end_datetime = " + end_datetime + ", job_title = " + job_title + "]";
        }
    }

}
