package com.ngnconstruction.Model.response;

import java.util.ArrayList;

public class JobDetailResponse {
    private String message;

    private String status;

    private ArrayList<Data> data;

    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", status = " + status + ", data = " + data + ", code = " + code + "]";
    }

    public class Data {
        private String job_number;

        private String created_by;

        private String jonInOutStatus;

        private String status;

        private String deleted_at;

        private String job_status;

        private String title;

        private String updated_at;

        private String end_time;

        private String description;

        private String duedate;

        private String start_time;

        private String created_at;

        private String client_id;

        private String notes;

        private Client_name client_name;

        private String job_id;

        public String getJob_number() {
            return job_number;
        }

        public void setJob_number(String job_number) {
            this.job_number = job_number;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getJonInOutStatus() {
            return jonInOutStatus;
        }

        public void setJonInOutStatus(String jonInOutStatus) {
            this.jonInOutStatus = jonInOutStatus;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(String deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getJob_status() {
            return job_status;
        }

        public void setJob_status(String job_status) {
            this.job_status = job_status;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDuedate() {
            return duedate;
        }

        public void setDuedate(String duedate) {
            this.duedate = duedate;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public Client_name getClient_name() {
            return client_name;
        }

        public void setClient_name(Client_name client_name) {
            this.client_name = client_name;
        }

        public String getJob_id() {
            return job_id;
        }

        public void setJob_id(String job_id) {
            this.job_id = job_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [job_number = " + job_number + ", created_by = " + created_by + ", jonInOutStatus = " + jonInOutStatus + ", status = " + status + ", deleted_at = " + deleted_at + ", job_status = " + job_status + ", title = " + title + ", updated_at = " + updated_at + ", end_time = " + end_time + ", description = " + description + ", duedate = " + duedate + ", start_time = " + start_time + ", created_at = " + created_at + ", client_id = " + client_id + ", notes = " + notes + ", client_name = " + client_name + ", job_id = " + job_id + "]";
        }
    }

    public class Client_name {
        private String id;

        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", name = " + name + "]";
        }
    }

}
