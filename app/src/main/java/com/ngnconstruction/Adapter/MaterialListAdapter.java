package com.ngnconstruction.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.SubModel.MaterialList;
import com.ngnconstruction.R;
import com.ngnconstruction.activity.MaterialDetailActivity;

import java.util.List;
import java.util.Locale;

public class MaterialListAdapter extends RecyclerView.Adapter<MaterialListAdapter.ViewHolder> {

    private List<MaterialList> data;
    private MaterialList MaterialList;
    private Context mContext;
    public MaterialListAdapter(List<MaterialList> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.content_material_item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        MaterialList=getItem(position);
        holder.txtMaterial.setText(MaterialList.getMaterialName());
        holder.txtQuantity.setText(String.format(Locale.getDefault(), "%d Qty.", MaterialList.getStock() == null ? 0 : MaterialList.getStock()));
        if (MaterialList.getImage() == null || TextUtils.isEmpty(MaterialList.getImage())) {
            holder.progress_bar.setVisibility(View.GONE);
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);


            Glide.with(mContext)
                    .load(MaterialList.getImage())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.ic_no_image))
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.progress_bar.setVisibility(View.GONE);
                            return false;

                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.progress_bar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.imgMaterial);
        }

        holder.bind(holder.getAdapterPosition(), String.valueOf(MaterialList.getId()),MaterialList.getMaterialName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public MaterialList getItem(int position) {
        return data.get(position);
    }


    public void add(List<MaterialList> items) {
        int previousDataSize = this.data.size();
        this.data.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        ImageView imgMaterial;
        ProgressBar progress_bar;
        public final TextView txtMaterial;
        public final TextView txtQuantity;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            imgMaterial = view.findViewById(R.id.imgMaterial);
            progress_bar = view.findViewById(R.id.progress_bar);
            txtMaterial = view.findViewById(R.id.txtMaterial);
            txtQuantity = view.findViewById(R.id.txtQuantity);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtMaterial.getText() + "'";
        }

        void bind(int position, final String id, final String materialName) {
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e( "onClick: ",id );
                    Intent intent = new Intent(mContext, MaterialDetailActivity.class);
                    intent.putExtra(AppConstants.RequestDataKey.id,id);
                    intent.putExtra("title",materialName );
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
