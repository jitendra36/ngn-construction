package com.ngnconstruction.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.ToolListActiveRequest;
import com.ngnconstruction.Model.response.SimpleResponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.fragment.ActiveToolFragment;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActiveToolAdapter extends RecyclerView.Adapter<ActiveToolAdapter.VeiwHolder> {

    private static final String TAG = ActiveToolAdapter.class.getSimpleName();
    View view;
    Context context;

    List<ToolListActiveRequest> list;
    ToolListActiveRequest toolsModel;
    SparseBooleanArray itemStateArray= new SparseBooleanArray();
    ApiInterfaceListener listener;
    private ActiveToolFragment fragment;


    public ActiveToolAdapter(Context context, List<ToolListActiveRequest> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public VeiwHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.content_tool_list_item, parent, false);
        return new VeiwHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VeiwHolder holder, int position) {
        toolsModel = getItem(position);
        holder.title.setText(toolsModel.getName());
        holder.box.setTag(position);
        holder.bind(position,toolsModel);

        if (toolsModel.getImage() == null || TextUtils.isEmpty(toolsModel.getImage())) {
            holder.progress_bar.setVisibility(View.GONE);
        } else {
            holder.progress_bar.setVisibility(View.VISIBLE);

            Glide.with(context)
                    .load(toolsModel.getImage())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.ic_no_image))
                    .addListener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.progress_bar.setVisibility(View.GONE);
                            return false;

                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.progress_bar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.imgTool);
        }

    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }



    public void selectAll() {
        for (int i = 0; i < getItemCount(); i++) {
            itemStateArray.put(i, true);

        }
        notifyDataSetChanged();
    }

    public void clearAll() {
        for (int i = 0; i < getItemCount(); i++) {
            itemStateArray.delete(i);

        }
        notifyDataSetChanged();
    }


    public void setFragment(ActiveToolFragment fragment) {
        this.fragment = fragment;
    }


    private boolean isCheckboxChecked() {
        return itemStateArray.size() > 0;
    }

    private boolean isAllCheckboxChecked() {
        return itemStateArray.size() == getItemCount();
    }

    public List<ToolListActiveRequest> getCheckedTool() {
        List<ToolListActiveRequest> temp = new ArrayList<>();
        for (int i = 0; i < itemStateArray.size(); i++) {
            int key = itemStateArray.keyAt(i);
            Log.e(TAG, "getCheckedTool: " + key);
            temp.add(list.get(key));
        }
        return temp;
    }

    public void clear() {
        list.clear();
    }

    public class VeiwHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ProgressBar progress_bar;
        TextView title;
        ImageView imgTool;
        CheckBox box;
        ToolListActiveRequest toolsModel;
        View relativeSelected;

        VeiwHolder(View itemView) {
            super(itemView);
            relativeSelected = itemView.findViewById(R.id.relativeSelected);

            progress_bar = itemView.findViewById(R.id.progress_bar);
            imgTool = itemView.findViewById(R.id.imgTool);
            title = itemView.findViewById(R.id.txtToolName);
            box = itemView.findViewById(R.id.cbTool);
            itemView.setOnClickListener(this);
        }

        void bind(int position, ToolListActiveRequest toolsModel) {
            this.toolsModel=toolsModel;
            if (!itemStateArray.get(position, false)) {

                box.setChecked(true);
                relativeSelected.setBackgroundColor(context.getResources().getColor(R.color.colorToolSelected));
            }
            else {
                box.setChecked(false);
                relativeSelected.setBackgroundColor(Color.TRANSPARENT);

            }

        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            if (!itemStateArray.get(adapterPosition, false)) {
                box.setChecked(true);
                itemStateArray.put(adapterPosition, true);
                relativeSelected.setBackgroundColor(context.getResources().getColor(R.color.colorToolSelected));

            } else  {
                box.setChecked(false);
                itemStateArray.delete(adapterPosition);
                relativeSelected.setBackgroundColor(Color.TRANSPARENT);

//                final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//                String dateAndTime=format.format(System.currentTimeMillis());
//                Log.e("onClick: ",dateAndTime );
//                getUpdate(String.valueOf(toolsModel.getId()),dateAndTime);

            }
            if (isCheckboxChecked()) {
                fragment.updateMenu(true);
            } else {
                fragment.updateMenu(false);

            }
            if (isAllCheckboxChecked()) {
                fragment.updateCheckBox(true);
            } else {
                fragment.updateCheckBox(false);
            }
        }

    }

    public ToolListActiveRequest getItem(int position) {
        return list.get(position);
    }

    private void getUpdate(String tool_id,String date_time) {
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<SimpleResponse> call = listener.checkInOutTools(user_id, api_token, tool_id, "1", date_time, "");
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                SimpleResponse simpleResponse = response.body();
                if (response.body().getStatus().equals("true")) {
                    Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    fragment.UpdateList();
                } else {
                    Toast.makeText(context, simpleResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }



}
