package com.ngnconstruction.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ngnconstruction.Model.response.JobTimingResponse;
import com.ngnconstruction.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobTimingAdapter extends RecyclerView.Adapter<JobTimingAdapter.JobTimingHolder> {

    private ArrayList<JobTimingResponse.Hoursdata> data;
    private Context mContext;

    public JobTimingAdapter(ArrayList<JobTimingResponse.Hoursdata> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public JobTimingHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.content_job_timing_list_item, viewGroup, false);
        return new JobTimingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JobTimingHolder holder, int position) {
        JobTimingResponse.Hoursdata hoursdata = data.get(position);
        holder.txtJobStartDate.setText(getConvertedDate(hoursdata.getStart_datetime(), "yyyy-MM-dd HH:mm:ss", "EEE, MMMM d,yyyy"));
        holder.txtJobEndDate.setText(getConvertedDate(hoursdata.getEnd_datetime(), "yyyy-MM-dd HH:mm:ss", "EEE, MMMM d,yyyy"));
        holder.txtJobStartTime.setText(getConvertedDate(hoursdata.getStart_datetime(), "yyyy-MM-dd HH:mm:ss", "HH:mm a"));
        holder.txtJobEndTime.setText(getConvertedDate(hoursdata.getEnd_datetime(), "yyyy-MM-dd HH:mm:ss", "HH:mm a"));
        holder.txtJobHour.setText(String.format(Locale.getDefault(), "%sh", hoursdata.getHours()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class JobTimingHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtJobStartDate)
        TextView txtJobStartDate;
        @BindView(R.id.txtJobEndDate)
        TextView txtJobEndDate;
        @BindView(R.id.txtJobStartTime)
        TextView txtJobStartTime;
        @BindView(R.id.txtJobEndTime)
        TextView txtJobEndTime;
        @BindView(R.id.txtJobHour)
        TextView txtJobHour;

        public JobTimingHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private String getConvertedDate(String inputDate, String inputFormat, String outputFormat) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }
}
