package com.ngnconstruction.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.JobfolderList;
import com.ngnconstruction.R;
import com.ngnconstruction.activity.FolderListActivity;

import java.util.List;

public class FolderListAdapter extends RecyclerView.Adapter<FolderListAdapter.ViewHolder> {
    Context mContext;
    List<JobfolderList> list;
    JobfolderList mJobfolderList;


    public FolderListAdapter(Context mContext, List<JobfolderList> list) {
        this.mContext = mContext;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.folder_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mJobfolderList = getItem(position);
        holder.layout.setVisibility(View.VISIBLE);
        holder.textView.setText(mJobfolderList.getFolderName());
        if (mJobfolderList.getFolderName().equalsIgnoreCase("Heating")) {
            holder.textView.setTextColor(mContext.getResources().getColor(R.color.colorHeating));
        } else if (mJobfolderList.getFolderName().equalsIgnoreCase("Cooling")) {
            holder.textView.setTextColor(mContext.getResources().getColor(R.color.colorCooling));
        } else if (mJobfolderList.getFolderName().equalsIgnoreCase("Plumbing")) {
            holder.textView.setTextColor(mContext.getResources().getColor(R.color.colorPlumbing));
        } else if (mJobfolderList.getFolderName().equalsIgnoreCase("Electrical")) {
            holder.textView.setTextColor(mContext.getResources().getColor(R.color.colorElectrical));
        } else if (mJobfolderList.getFolderName().equalsIgnoreCase("Construction")) {
            holder.textView.setTextColor(mContext.getResources().getColor(R.color.colorConstruction));
        } else if (mJobfolderList.getFolderName().equalsIgnoreCase("Renovation")) {
            holder.textView.setTextColor(mContext.getResources().getColor(R.color.colorRenovation));
        }

        holder.layout.setTag(position);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout ll = (LinearLayout) view;
                if (ll.getTag() == view.getTag()) {
                    Intent intent = new Intent(mContext, FolderListActivity.class);
                    intent.putExtra(AppConstants.RequestDataKey.folder_id, String.valueOf(list.get(Integer.parseInt("" + ll.getTag())).getId()));
                    intent.putExtra(AppConstants.RequestDataKey.job_id, String.valueOf(list.get(Integer.parseInt("" + ll.getTag())).getJobId()));
                    intent.putExtra("title", String.valueOf(list.get(Integer.parseInt("" + ll.getTag())).getFolderName()));
                    mContext.startActivity(intent);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public JobfolderList getItem(int position) {
        return list.get(position);
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        LinearLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.folder);
            layout = itemView.findViewById(R.id.folder_select);
        }
    }
}
