package com.ngnconstruction.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.JobListRequest;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.activity.HomeActivity;
import com.ngnconstruction.activity.JobTimingActivity;

import java.util.List;

public class JobListAdapter extends RecyclerView.Adapter<JobListAdapter.ViewHolder> {
    Context context;
    private JobListRequest jobList = new JobListRequest();
    List<JobListRequest> list;
    View view;

    public JobListAdapter(List<JobListRequest> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.job_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        jobList = getItem(position);

        holder.job.setText(jobList.getTitle());
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        if (jobList.getJobflag()==1) {
            holder.job_icon.setVisibility(View.VISIBLE);
            holder.imgbtnJobTimings.setVisibility(View.VISIBLE);
            holder.relativeBg.setBackgroundColor(context.getResources().getColor(R.color.colorToolSelected));
            holder.ll.setTag(position);
            holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CardView job = (CardView) view;
                    if (job.getTag() == view.getTag()) {
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtra(AppConstants.RequestDataKey.id, String.valueOf(list.get(Integer.parseInt(("" + job.getTag()))).getJobId()));
                        intent.putExtra("title", String.valueOf(list.get(Integer.parseInt(("" + job.getTag()))).getTitle()));
                        context.startActivity(intent);
                    }

                }
            });
        } else {
            holder.job_icon.setVisibility(View.INVISIBLE);
            holder.imgbtnJobTimings.setVisibility(View.INVISIBLE);
            holder.relativeBg.setBackgroundColor(Color.TRANSPARENT);
        }
        holder.bind(position, jobList);

    }

    public void add(List<JobListRequest> items) {
        int previousDataSize = this.list.size();
        this.list.addAll(items);
        notifyItemRangeInserted(previousDataSize, items.size());
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public JobListRequest getItem(int position) {
        return list.get(position);
    }

    public void updateList(List<JobListRequest> temp) {
        list = temp;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View relativeBg;
        TextView job;
        CardView ll;
        ImageView job_icon;
        ImageButton imgbtnJobTimings;

        public ViewHolder(View itemView) {
            super(itemView);

            relativeBg = view.findViewById(R.id.relativeBg);
            job = view.findViewById(R.id.job_namne);
            ll = view.findViewById(R.id.linearjob);
            job_icon = view.findViewById(R.id.job_icon);
            imgbtnJobTimings = view.findViewById(R.id.imgbtnJobTimings);
        }

        public void bind(int position, JobListRequest jobList) {
            imgbtnJobTimings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, JobTimingActivity.class);
                    intent.putExtra(IntentConstants.JOB_ID, jobList.getJobId());
                    intent.putExtra(IntentConstants.JOB_NAME, jobList.getTitle());
                    intent.putExtra(IntentConstants.JOB_IN_OUT_STATUS, jobList.getJonInOutStatus());
                    context.startActivity(intent);
                }
            });
        }
    }

}
