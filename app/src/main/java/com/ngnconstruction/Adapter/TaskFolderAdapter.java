package com.ngnconstruction.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.JobfolderList;
import com.ngnconstruction.R;
import com.ngnconstruction.activity.TaskJobList;

import java.util.List;

public class TaskFolderAdapter extends RecyclerView.Adapter<TaskFolderAdapter.VeiwHolder> {
    View view;
    Context context;
    JobfolderList jobfolderList;
    List<JobfolderList> jobfolder;


    public TaskFolderAdapter(Context context, List<JobfolderList> jobfolder) {
        this.context = context;
        this.jobfolder = jobfolder;
    }


    @Override
    public VeiwHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.folder_list, parent, false);
        return new VeiwHolder(view);
    }


    @Override
    public void onBindViewHolder(final VeiwHolder holder, int position) {
        jobfolderList = getItem(position);
        (holder).folder.setText(jobfolderList.getFolderName());

        holder.layout.setTag(position);
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout layout = (LinearLayout) view;
                if (layout.getTag() == view.getTag())
                {
                    Intent intent=new Intent(context,TaskJobList.class);
                    intent.putExtra(AppConstants.RequestDataKey.job_id, String.valueOf(jobfolder.get(Integer.parseInt(""+layout.getTag())).getJobId()));
                    intent.putExtra(AppConstants.RequestDataKey.folder_id, String.valueOf(jobfolder.get(Integer.parseInt(""+layout.getTag())).getId()));
                    intent.putExtra("title", String.valueOf(jobfolder.get(Integer.parseInt(""+layout.getTag())).getFolderName()));
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return jobfolder.size();
    }

    public class VeiwHolder extends RecyclerView.ViewHolder {
        TextView folder;
        LinearLayout layout;

        public VeiwHolder(View itemView) {
            super(itemView);
            folder = itemView.findViewById(R.id.folder);
            layout = itemView.findViewById(R.id.folder_select);
            layout.setVisibility(View.VISIBLE);
        }
    }

    public JobfolderList getItem(int position) {
        return jobfolder.get(position);
    }


}
