package com.ngnconstruction.Utility;

import android.content.Intent;

public class IntentConstants {
    public static final String ACTION_MATERIAL = "ACTION_MATERIAL";
    public static final String ACTION_ACTIVE_TOOLS = "ACTION_ACTIVE_TOOLS";
    public static final String ACTION_AVAILABLE_TOOLS = "ACTION_AVAILABLE_TOOLS";
    public static final String START_DATE = "STARTDATE";
    public static final String END_DATE = "ENDATE";
    public static final String JOB_ID = "JOB_ID";
    public static final String JOB_NAME = "JOB_NAME";
    public static final String JOB_IN_OUT_STATUS = "JOB_IN_OUT_STATUS";
    public static final String ACTION_JOB_LIST_UPDATE = "ACTION_JOB_LIST_UPDATE";
}
