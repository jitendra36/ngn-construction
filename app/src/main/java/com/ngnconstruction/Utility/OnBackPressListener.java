package com.ngnconstruction.Utility;

public interface OnBackPressListener {
    boolean onBackPress();
}
