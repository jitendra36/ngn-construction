package com.ngnconstruction.fragment;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ngnconstruction.Adapter.ActiveToolAdapter;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.ToolListActiveRequest;
import com.ngnconstruction.Model.response.SimpleResponse;
import com.ngnconstruction.Model.response.ToolListActiveResponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.MessageUtil;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import org.json.JSONArray;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActiveToolFragment extends Fragment {


    private static final String TAG = ActiveToolFragment.class.getSimpleName();
    @BindView(R.id.recyclerTools)
    RecyclerView recyclerTools;
    Unbinder unbinder;
    @BindView(R.id.txtNoData)
    TextView txtNoData;
    @BindView(R.id.cbSelectAll)
    CheckBox cbSelectAll;
    @BindView(R.id.layoutSelectAll)
    LinearLayout layoutSelectAll;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private ProgressDialog mProgressDialog;
    private Menu menuTool;
    private boolean isVisible = false;
    private String location = "";
    List<ToolListActiveRequest> mToolList;
    private ApiInterfaceListener listener;
    private ActiveToolAdapter mActiveToolAdapter;
    private BroadcastReceiver mBroadcastReceiver;

    public ActiveToolFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tools, container, false);
        unbinder = ButterKnife.bind(this, view);
        mToolList = new ArrayList<ToolListActiveRequest>();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        initDialog();
        initRecyclview();
        initBroadCast();
        if (Network.isConnected(getActivity())) {
            toolsUsedList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }
        setRegisterListener();
        return view;
    }

    private void initBroadCast() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                mActiveToolAdapter.clear();
                toolsUsedList();
            }

        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver, new IntentFilter(IntentConstants.ACTION_ACTIVE_TOOLS));
    }

    private void setRegisterListener() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Network.isConnected(getActivity())) {

                    mActiveToolAdapter.clear();
                    toolsUsedList();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
    }

    private void initRecyclview() {
        recyclerTools.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mActiveToolAdapter = new ActiveToolAdapter(getActivity(), mToolList);
        mActiveToolAdapter.setFragment(this);
        recyclerTools.setAdapter(mActiveToolAdapter);
    }


    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_tool, menu);
        menuTool = menu;
        menu.findItem(R.id.menuDone).setVisible(isVisible);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDone:
                showLocationDialog();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void toolsUsedList() {
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        swipeRefreshLayout.setRefreshing(true);
        Call<ToolListActiveResponse> call = listener.toolsUsedList(user_id, api_token);
        call.enqueue(new Callback<ToolListActiveResponse>() {
            @Override
            public void onResponse(Call<ToolListActiveResponse> call, Response<ToolListActiveResponse> response) {
                if (response.isSuccessful()) {
                    ToolListActiveResponse request = response.body();
                    if (request != null) {
                        switch (request.getCode()) {
                            case 200:
                                if (request.getData().size() > 0) {
                                    mToolList.addAll(request.getData());
                                    mActiveToolAdapter.notifyDataSetChanged();
                                } else {
                                    recyclerTools.setVisibility(View.GONE);
                                    txtNoData.setVisibility(View.VISIBLE);
                                }
                                break;
                            default:
                                recyclerTools.setVisibility(View.GONE);
                                txtNoData.setVisibility(View.VISIBLE);
                                break;
                        }


                    } else {
                        recyclerTools.setVisibility(View.GONE);
                        txtNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: "+response.errorBody().string() );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ToolListActiveResponse> call, Throwable t) {

                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    public void UpdateList() {

        toolsUsedList();
    }

    public void updateCheckBox(boolean checkboxChecked) {
        cbSelectAll.setChecked(checkboxChecked);

    }

    public void updateMenu(boolean visible) {
        isVisible = visible;
        if (menuTool != null) {
            MenuItem item = menuTool.findItem(R.id.menuDone);
            item.setVisible(visible);
            getActivity().invalidateOptionsMenu();
        }

    }

    private void showLocationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Location");
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.layout_location, (ViewGroup) getView(), false);
        final TextInputEditText input = viewInflated.findViewById(R.id.edtLocation);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                location = input.getText().toString();
                if (mActiveToolAdapter.getCheckedTool().size() > 1) {
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < mActiveToolAdapter.getCheckedTool().size(); i++) {
                        jsonArray.put(mActiveToolAdapter.getCheckedTool().get(i).getId());
                    }

                    Log.e(TAG, "onClick: "+jsonArray.toString() );
                    postUpdateTool(jsonArray.toString(),location);
                } else {
                    postUpdateTool(String.valueOf(mActiveToolAdapter.getCheckedTool().get(0).getId()),location);
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void postUpdateTool(String toolId, String location) {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String dateAndTime = format.format(System.currentTimeMillis());

        getUpdate(toolId, dateAndTime, location);
    }

    private void getUpdate(String tool_id, String date_time,String location) {
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
mProgressDialog.show();
        Call<SimpleResponse> call = listener.checkInOutTools(user_id, api_token, tool_id, "1", date_time,location);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                if (response.isSuccessful()) {
                    SimpleResponse simpleResponse = response.body();
                    switch (simpleResponse.getCode()) {
                        case "200":
                            MessageUtil.showToast(getActivity(), simpleResponse.getMessage());
mActiveToolAdapter.clear();
                            mActiveToolAdapter.selectAll();
                            updateMenu(false);
                            toolsUsedList();
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(IntentConstants.ACTION_AVAILABLE_TOOLS));
                            break;
                        default:
                            MessageUtil.showToast(getActivity(), simpleResponse.getMessage());
                            break;
                    }

                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                t.printStackTrace();
                mProgressDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.cbSelectAll)
    public void onViewClicked() {
    }
}
