package com.ngnconstruction.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ngnconstruction.Adapter.FolderListAdapter;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.FolderListRequest;
import com.ngnconstruction.Model.request.JobfolderList;
import com.ngnconstruction.Model.response.FolderListResponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.activity.HomeActivity;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Folder extends Fragment {

    View view;
    RecyclerView recyclerView;
    ApiInterfaceListener listener;
    Context context;
    String job_id;
    ProgressDialog mProgressDialog;
    FolderListAdapter mFolderListAdapter;
    private GridLayoutManager lLayoutPost;
    private SwipeRefreshLayout swiperefresh;
    private ArrayList<JobfolderList> list;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.folder_fragmemt, container, false);
        list = new ArrayList<>();
        recyclerView = view.findViewById(R.id.folder_list);
        swiperefresh = view.findViewById(R.id.swiperefresh);

        FloatingActionButton button = view.findViewById(R.id.vid_flot);
        button.setVisibility(View.GONE);

        context = getContext();
        AppConstants.CONTEXT = context;
        initDialog();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        HomeActivity activity = (HomeActivity) getActivity();
        job_id = activity.getId();

        if (Network.isConnected(getActivity())) {
            listFolder(false);
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }
        initRecyerview();
        setRegisterListener();
        return view;
    }

    private void initRecyerview() {
        lLayoutPost = new GridLayoutManager(getActivity(), 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(lLayoutPost);
        mFolderListAdapter = new FolderListAdapter(getActivity(),list);
        recyclerView.setAdapter(mFolderListAdapter);
    }

    private void setRegisterListener() {
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Network.isConnected(getActivity())) {

                    listFolder(true);
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void listFolder(boolean isSwipe) {
        if (isSwipe) {
            mFolderListAdapter.clear();
        }
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<FolderListResponse> call = listener.folderList(user_id, api_token, job_id);
        call.enqueue(new Callback<FolderListResponse>() {
            @Override
            public void onResponse(Call<FolderListResponse> call, Response<FolderListResponse> response) {
               mProgressDialog.dismiss();
                if (response.isSuccessful() && response.body() != null)
                {
                    swiperefresh.setRefreshing(true);
                    FolderListResponse listResponse = response.body();
                    List<FolderListRequest> folderList = listResponse.getData();
                    Log.e("folderList", folderList.toString());

                    for (int i = 0; i < folderList.size(); i++) {
                        FolderListRequest listRequest = folderList.get(i);

                        list.addAll(listRequest.getJobfolderList());
                    }
                    mFolderListAdapter.notifyDataSetChanged();
                    swiperefresh.setRefreshing(false);
                } else {
                    try {
                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<FolderListResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

}
