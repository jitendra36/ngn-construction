package com.ngnconstruction.fragment;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ngnconstruction.Adapter.AvailableToolAdapter;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.ToolListInactiveRequest;
import com.ngnconstruction.Model.response.SimpleResponse;
import com.ngnconstruction.Model.response.ToolListInactiveResponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.MessageUtil;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import org.json.JSONArray;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvailableToolsFragment extends Fragment implements AvailableToolAdapter.OnItemCheckListener {


    private static final String TAG = AvailableToolsFragment.class.getSimpleName();
    @BindView(R.id.recyclerTools)
    RecyclerView recyclerTools;
    Unbinder unbinder;

    @BindView(R.id.txtNoData)
    TextView txtNoData;
    @BindView(R.id.cbSelectAll)
    CheckBox cbSelectAll;

    @BindView(R.id.layoutSelectAll)
    LinearLayout layoutSelectAll;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private ProgressDialog mProgressDialog;
    private AvailableToolAdapter mAvailableToolAdapter;
    private List<ToolListInactiveRequest> mToolList;
    private ApiInterfaceListener listener;
    private Menu menuTool;
    private boolean isVisible = false;
    private String location = "";
    private BroadcastReceiver mBroadcastReceiver;
    private List<ToolListInactiveRequest> currentSelectedItems = new ArrayList<>();

    public AvailableToolsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tools, container, false);
        unbinder = ButterKnife.bind(this, view);
        mToolList = new ArrayList<>();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        initDialog();
        initRecyclview();
        initBroadCast();
        if (Network.isConnected(Objects.requireNonNull(getActivity()))) {
            toolsAvailableList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }
        setRegisterListener();
        return view;
    }

    private void setRegisterListener() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Network.isConnected(Objects.requireNonNull(getActivity()))) {
                    mAvailableToolAdapter.clearAll();
                    mAvailableToolAdapter.clear();
                    toolsAvailableList();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_tool, menu);
        menuTool = menu;
        menu.findItem(R.id.menuDone).setVisible(isVisible);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuDone:
                showLocationDialog();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void initBroadCast() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mAvailableToolAdapter.clear();
                toolsAvailableList();
            }
        };
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).registerReceiver(mBroadcastReceiver, new IntentFilter(IntentConstants.ACTION_AVAILABLE_TOOLS));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).unregisterReceiver(mBroadcastReceiver);
    }

    private void showLocationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Location");
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.layout_location, (ViewGroup) getView(), false);
        final TextInputEditText input = viewInflated.findViewById(R.id.edtLocation);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                location = input.getText().toString();
                dialog.dismiss();
                if (currentSelectedItems.size() > 1) {
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < currentSelectedItems.size(); i++) {
                        jsonArray.put(currentSelectedItems.get(i).getId());
                    }

                    Log.e(TAG, "onClick: " + jsonArray.toString());
                    postUpdateTool(jsonArray.toString(), location);
                } else {
                    postUpdateTool(String.valueOf(currentSelectedItems.get(0).getId()), location);
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void postUpdateTool(String toolsModel, String location) {
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String dateAndTime = format.format(System.currentTimeMillis());

        getUpdate(toolsModel, dateAndTime, location);
    }

    private void getUpdate(String tool_id, String date_time, String location) {
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        mProgressDialog.show();
        Call<SimpleResponse> call = listener.checkInOutTools(user_id, api_token, tool_id, "0", date_time, location);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                if (response.isSuccessful()) {
                    SimpleResponse simpleResponse = response.body();
                    assert simpleResponse != null;
                    switch (simpleResponse.getCode()) {
                        case "200":
                            MessageUtil.showToast(getActivity(), simpleResponse.getMessage());
                            mAvailableToolAdapter.clearAll();
                            mAvailableToolAdapter.clear();

                            updateMenu(false);
                            toolsAvailableList();
                            LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).sendBroadcast(new Intent(IntentConstants.ACTION_ACTIVE_TOOLS));

                            break;
                        default:
                            MessageUtil.showToast(getActivity(), simpleResponse.getMessage());
                            break;
                    }

                } else {
                    try {
                        assert response.errorBody() != null;
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                t.printStackTrace();
                mProgressDialog.dismiss();
            }
        });
    }

    private void initRecyclview() {
        mAvailableToolAdapter = new AvailableToolAdapter(getActivity(), mToolList, this);
        mAvailableToolAdapter.setFragment(this);
        recyclerTools.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerTools.setAdapter(mAvailableToolAdapter);
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    public void updateMenu(boolean visible) {
        isVisible = visible;
        if (menuTool != null) {
            MenuItem item = menuTool.findItem(R.id.menuDone);
            item.setVisible(visible);
            getActivity().invalidateOptionsMenu();
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void toolsAvailableList() {
        swipeRefreshLayout.setRefreshing(true);
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<ToolListInactiveResponse> call = listener.toolsAvailableList(user_id, api_token);
        call.enqueue(new Callback<ToolListInactiveResponse>() {
            @Override
            public void onResponse(Call<ToolListInactiveResponse> call, Response<ToolListInactiveResponse> response) {

                ToolListInactiveResponse request = response.body();
                if (request != null) {

                    if (request.getData().size() > 0) {
                        mToolList.addAll(request.getData());
                        mAvailableToolAdapter.notifyDataSetChanged();
                        layoutSelectAll.setEnabled(true);
                    } else {
                        layoutSelectAll.setEnabled(false);
                        recyclerTools.setVisibility(View.GONE);
                        txtNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    recyclerTools.setVisibility(View.GONE);
                    txtNoData.setVisibility(View.VISIBLE);

                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ToolListInactiveResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void UpdateList() {
        toolsAvailableList();
    }


    @OnClick({R.id.cbSelectAll})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cbSelectAll:
                if (mAvailableToolAdapter.getItemCount() > 0) {
                    if (cbSelectAll.isChecked()) {

                        mAvailableToolAdapter.selectAll();
                    } else {

                        mAvailableToolAdapter.clearAll();
                    }


                    if (mAvailableToolAdapter.isCheckboxChecked()) {
                        updateMenu(true);
                    } else {
                        updateMenu(false);
                    }
                }

                break;

        }
    }

    public void updateCheckBox(boolean checkboxChecked) {
        cbSelectAll.setChecked(checkboxChecked);

    }

    @Override
    public void onItemCheck(ToolListInactiveRequest toolsModel) {
        currentSelectedItems.add(toolsModel);

    }

    @Override
    public void onItemUncheck(ToolListInactiveRequest toolsModel) {
        currentSelectedItems.remove(toolsModel);
    }
}
