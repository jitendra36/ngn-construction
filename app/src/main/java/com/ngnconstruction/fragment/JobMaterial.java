package com.ngnconstruction.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.response.MaterialListWithoutPageResponse;
import com.ngnconstruction.Model.response.StockUpdateResponce;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.MessageUtil;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobMaterial extends Fragment {


    private static final String TAG = JobMaterial.class.getSimpleName();
    @BindView(R.id.spMaterialList)
    Spinner spMaterialList;
    @BindView(R.id.edtQuantity)
    TextInputEditText edtQuantity;
    @BindView(R.id.edtDescription)
    TextInputEditText edtDescription;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    Unbinder unbinder;
    private ApiInterfaceListener listener;
    private List<MaterialListWithoutPageResponse.Data> mMaterials;
    private ProgressDialog mProgressDialog;
    public JobMaterial() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_job_material, container, false);
        unbinder = ButterKnife.bind(this, view);
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        initDialog();
        if (Network.isConnected(getActivity())) {
            loadList(false);
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait..");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

    }

    private void loadList(boolean broadcast) {

        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        retrofit2.Call<MaterialListWithoutPageResponse> call = listener.listMaterialWithoutPage(user_id, api_token, "1");
        call.enqueue(new Callback<MaterialListWithoutPageResponse>() {
            @Override
            public void onResponse(retrofit2.Call<MaterialListWithoutPageResponse> call, Response<MaterialListWithoutPageResponse> response) {

                MaterialListWithoutPageResponse request = response.body();
                if (request != null) {

                    mMaterials = request.getData();

                    if (mMaterials.size() > 0) {
                        setupSpinner(mMaterials);
                    }

                }
            }

            @Override
            public void onFailure(retrofit2.Call<MaterialListWithoutPageResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void setupSpinner(List<MaterialListWithoutPageResponse.Data> mMaterials) {
        ArrayAdapter<MaterialListWithoutPageResponse.Data> adapter = new ArrayAdapter<>(Objects.requireNonNull(getActivity()), R.layout.layout_spinner_item, mMaterials);
        adapter.setDropDownViewResource(R.layout.layout_spinner_item);
        spMaterialList.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        String qty = edtQuantity.getText().toString().trim();
        String desc = edtDescription.getText().toString().trim();
        String materialId = "";
        MaterialListWithoutPageResponse.Data materialList = (MaterialListWithoutPageResponse.Data) spMaterialList.getSelectedItem();
        if (materialList != null) {
            materialId = materialList.getMaterial_id();
            if (Network.isConnected(Objects.requireNonNull(getActivity()))) {
                postSubmit(materialId, qty, desc);
            } else {
                Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void postSubmit(String mt_id, String qty, String desc) {
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<StockUpdateResponce> call = listener.getMaterialUsed(user_id, api_token, mt_id, qty, desc);
        mProgressDialog.show();
        call.enqueue(new Callback<StockUpdateResponce>() {
            @Override
            public void onResponse(Call<StockUpdateResponce> call, Response<StockUpdateResponce> response) {

                if (response.isSuccessful()) {

                    StockUpdateResponce detailResponse = response.body();
                    assert detailResponse != null;
                    switch (detailResponse.getCode()) {
                        case 200:
                            edtQuantity.setText("");
                            edtDescription.setText("");
                            MessageUtil.showToast(getActivity(), detailResponse.getMessage());
                            break;
                        default:
                            edtQuantity.setText("");
                            edtDescription.setText("");
                            MessageUtil.showToast(getActivity(), detailResponse.getMessage());
                            break;
                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<StockUpdateResponce> call, Throwable t) {
                mProgressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
