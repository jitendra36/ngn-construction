package com.ngnconstruction.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ngnconstruction.Adapter.JobListAdapter;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.JobListData;
import com.ngnconstruction.Model.request.JobListRequest;
import com.ngnconstruction.Model.response.JobListRersponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.activity.LoginActivity;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;
import com.ngnconstruction.paginate.Paginate;
import com.ngnconstruction.recycler.LoadingListItemCreator;
import com.ngnconstruction.recycler.LoadingListItemSpanLookup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class ActiveJobFragment extends Fragment implements Paginate.Callbacks {
    View view;
    EditText search;
    RecyclerView recyclerView;
    SwipeRefreshLayout swiperefresh;
    Context context;
    String text="";
    ApiInterfaceListener listener;
    JobListAdapter adapter;
    LinearLayoutManager llm;
    private ProgressDialog mProgressDialog;
    int totalPageFromApi;
    List<JobListRequest> data;
    private boolean loading = false;
    private int page;
    protected long networkDelay = 2000;
    protected int totalPages;
    private Handler handler;
    protected int threshold = 4;
    private Paginate paginate;
    protected boolean customLoadingListItem = false;
    private static final int GRID_SPAN = 3;
    protected boolean addLoadingRow = true;
    private BroadcastReceiver mBroadcastReceiver;
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.active_job_fragment, container, false);

        search = view.findViewById(R.id.search);
        recyclerView = view.findViewById(R.id.recycler_home);
        swiperefresh = view.findViewById(R.id.swiperefresh);

        handler = new Handler();

        intiRecycview();
        context = getContext();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        initDialog();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void afterTextChanged(Editable editable) {
                    filter(editable.toString());
            }
        });


        if (Network.isConnected(getActivity())) {
            loadList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }
        initReceiver();
        setRegisterListener();
        return view;
    }

    private void initReceiver() {
        mBroadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadList();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver,new IntentFilter(IntentConstants.ACTION_JOB_LIST_UPDATE));
    }

    private void setRegisterListener() {
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (Network.isConnected(getActivity())) {
                    loadList();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void intiRecycview() {
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(llm);
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }
   private void loadList() {
//        mProgressDialog.show();
       swiperefresh.setRefreshing(true);
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

       retrofit2.Call<JobListRersponse> call = listener.jobList(user_id, api_token, "", text);
       call.enqueue(new Callback<JobListRersponse>() {
            @Override
            public void onResponse(retrofit2.Call<JobListRersponse> call, Response<JobListRersponse> response) {
//                mProgressDialog.dismiss();
                swiperefresh.setRefreshing(false);
                if (response.isSuccessful()) {
                    JobListRersponse request = response.body();


                        if (request != null) {
                            JobListData listData = request.getData();
                            totalPageFromApi = listData.getLastPage();
                            page = 1;
                            hasLoadedAllItems();
                            Log.e("JobListRequest", request.getData().toString());
                            data = listData.getData();
                            setupPagination(data);
                        }


                } else {
                    try {

                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }



            }

            @Override
            public void onFailure(retrofit2.Call<JobListRersponse> call, Throwable t) {
//                mProgressDialog.dismiss();
                swiperefresh.setRefreshing(false);
            }
        });
    }

    private void logout() {

        Pref.setValue(AppConstants.preDataKey.user_id, "");
        Pref.setValue(AppConstants.preDataKey.api_token, "");
        Pref.setValue(AppConstants.preDataKey.name, "");
        Pref.setValue(AppConstants.preDataKey.email, "");
        Pref.setValue(AppConstants.logStatus, "");

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        Toast.makeText(getActivity(), "session expire please relogin  ", Toast.LENGTH_SHORT).show();


    }


    private void setpaginateData(int page) {
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        retrofit2.Call<JobListRersponse> call = listener.jobList(user_id, api_token, String.valueOf(page), text);
        call.enqueue(new Callback<JobListRersponse>() {
            @Override
            public void onResponse(retrofit2.Call<JobListRersponse> call, Response<JobListRersponse> response) {
                if (response.isSuccessful()) {
                    JobListRersponse request = response.body();

                    if (request != null) {
                        JobListData listData = request.getData();
                        List<JobListRequest> data = listData.getData();
                        if (data.size() > 0) {
                            adapter.add(data);
                            loading = false;
                        } else {
                            loading = true;
                        }
                    }
                } else {
                    try {
                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(retrofit2.Call<JobListRersponse> call, Throwable t) {
            }
        });
    }

    protected void setupPagination(List<JobListRequest> list) {
        if (paginate != null) {
            paginate.unbind();
        }
        handler.removeCallbacks(fakeCallback);
        adapter = new JobListAdapter( list);
        loading = false;


        recyclerView.setAdapter(adapter);
        Log.e("totalPageFromApi", String.valueOf(totalPageFromApi));
        if (totalPageFromApi > 1) {

            paginate = Paginate.with(recyclerView, this)
                    .setLoadingTriggerThreshold(threshold)
                    .addLoadingListItem(addLoadingRow)
                    .setLoadingListItemCreator(customLoadingListItem ? new CustomLoadingListItemCreator() : null)
                    .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                        @Override
                        public int getSpanSize() {
                            return GRID_SPAN;
                        }
                    })
                    .build();
        }
    }

    @Override
    public synchronized void onLoadMore() {
        Log.d("Paginate", "onLoadMore");
        loading = true;
        handler.postDelayed(fakeCallback, networkDelay);
    }

    @Override
    public synchronized boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        Log.e("page", String.valueOf(page));
        totalPages = totalPageFromApi;
        return page == totalPages;
    }

    private Runnable fakeCallback = new Runnable() {
        @Override
        public void run() {
            {
                page++;

                if (page < totalPages + 1) {
                    Log.e("page", String.valueOf(page));
                    if (Network.isConnected(getActivity())) {
                        setpaginateData(page);
                    } else {
                        Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    loading = true;
                }
            }
        }
    };


    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.custom_loading_list_item, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            VH vh = (VH) holder;
            vh.tvLoading.setText(String.format("Total items loaded: %d.\nLoading more...", adapter.getItemCount()));
            if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {
                StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) vh.itemView.getLayoutParams();
                params.setFullSpan(true);
            }
        }
    }

    static class VH extends RecyclerView.ViewHolder {
        TextView tvLoading;

        public VH(View itemView) {
            super(itemView);
            tvLoading =  itemView.findViewById(R.id.tv_loading_text);
        }
    }
    void filter(String text)
    {
        List<JobListRequest> temp = new ArrayList<JobListRequest>();
        if (data != null) {
            for(JobListRequest d: data )
            {
                if(d.getTitle().toLowerCase().contains(text.toLowerCase()))
                {
                    temp.add(d);
                }
            }
            adapter.updateList(temp);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
    }
}
