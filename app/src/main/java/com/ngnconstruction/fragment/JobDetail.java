package com.ngnconstruction.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.JobDetailRequest;
import com.ngnconstruction.Model.response.JobDetailResponse;
import com.ngnconstruction.Model.response.JobMarkResponse;
import com.ngnconstruction.Model.response.SimpleResponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.activity.HomeActivity;
import com.ngnconstruction.activity.JobTimingActivity;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobDetail extends Fragment {
    View view;
    TextView date, desc, title,txtTime,txtJobNumber,txtClientName,txtNotes;
    Button done,btnJobStatus;
    ImageButton timeButton;
    Context context;
    ApiInterfaceListener listener;
    String id;
    ProgressDialog mProgressDialog;
    String status;
    String time="";


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.job_detail_fragment, container, false);
        title = view.findViewById(R.id.label);
        txtJobNumber = view.findViewById(R.id.txtJobNumber);
        txtClientName = view.findViewById(R.id.txtClientName);
        txtNotes = view.findViewById(R.id.txtNotes);
        date = view.findViewById(R.id.date_lable);
        desc = view.findViewById(R.id.label_de);
        done = view.findViewById(R.id.mark);
        txtTime = view.findViewById(R.id.txtTime);
        btnJobStatus = view.findViewById(R.id.btnJobStatus);
        timeButton = view.findViewById(R.id.imageButton);

        context = getContext();
        AppConstants.CONTEXT = context;
        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        HomeActivity activity = (HomeActivity) getActivity();
        id= activity.getId();
        initDialog();
        if (Network.isConnected(getActivity())) {
            jobDetail();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Network.isConnected(getActivity())) {
                    jobUpdate();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        timeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimePicker();
            }
        });
        btnJobStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!time.equalsIgnoreCase(""))
                {
                    if (status.equals("0")){
                        checkoutIn("0",time);
                    }
                    else {
                        checkoutIn("1",time);
                    }
                }
                else {
                    Toast.makeText(getContext(),"Please Select Date & Time First", Toast.LENGTH_SHORT).show();
                }



            }
        });

        return view;

    }


        private void showDateTimePicker() {
            final Calendar currentDate = Calendar.getInstance();
            final Calendar date = Calendar.getInstance();
            final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            final SimpleDateFormat formatShow = new SimpleDateFormat("yyyy/MM/dd hh:mm aaa", Locale.getDefault());
            DatePickerDialog datePickerDialog=   new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    date.set(year, monthOfYear, dayOfMonth);
                    new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            date.set(Calendar.MINUTE, minute);
                            time=(format.format(date.getTime()));
                            txtTime.setText(formatShow.format(date.getTime()));
                        }
                    }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
                }
            }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
            datePickerDialog.show();
        }

    private void checkoutIn(String flag,String getTime) {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<SimpleResponse> call = listener.checkInOutJob(user_id,api_token,id,flag,getTime);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                mProgressDialog.dismiss();
                SimpleResponse detailResponse = response.body();
                Toast.makeText(getContext(), detailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                time="";
                jobDetail();
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(IntentConstants.ACTION_JOB_LIST_UPDATE));
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
            }
        });

    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }
    private void jobUpdate()
    {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<JobMarkResponse> call=listener.markAsDone(user_id, api_token, id);
        call.enqueue(new Callback<JobMarkResponse>() {
            @Override
            public void onResponse(Call<JobMarkResponse> call, Response<JobMarkResponse> response)
            {
                mProgressDialog.dismiss();
                if (response.body().getStatus().equals("true"))
                {
                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    mProgressDialog.dismiss();
                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<JobMarkResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void jobDetail() {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<JobDetailResponse> call = listener.jobDetail(api_token, id, user_id);
        call.enqueue(new Callback<JobDetailResponse>() {
            @Override
            public void onResponse(Call<JobDetailResponse> call, Response<JobDetailResponse> response) {
                mProgressDialog.dismiss();

                if (response.isSuccessful() && response.body() != null) {
                    JobDetailResponse detailResponse = response.body();
                    List<JobDetailResponse.Data> requests = detailResponse.getData();

                    for (int i = 0; i < requests.size(); i++) {
                        JobDetailResponse.Data object = requests.get(i);
                        String label = object.getTitle();
                        String d_date = object.getDuedate();
                        String des = object.getDescription();
                        String job_number = object.getJob_number();
                        String client_name = object.getClient_name().getName();
                        String notes = object.getNotes();
                        if (object.getJonInOutStatus() != null) {

                            status = object.getJonInOutStatus();
                            if (object.getJonInOutStatus().equalsIgnoreCase(String.valueOf(0))) {
                                btnJobStatus.setTag("in");
                                btnJobStatus.setText("Status in");
                            } else {
                                btnJobStatus.setTag("out");
                                btnJobStatus.setText("Status out");
                            }
                        } else {
                            btnJobStatus.setTag("out");
                            btnJobStatus.setText("Status out");
                        }
                        title.setText(label);
                        date.setText(d_date);
                        desc.setText(des);
                        txtJobNumber.setText(job_number);
                        txtClientName.setText(client_name);
                        txtNotes.setText(notes);

                    }

                } else {
                    try {
                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JobDetailResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
            }
        });

    }
}
