package com.ngnconstruction.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ngnconstruction.Adapter.MaterialListAdapter;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.SubModel.MaterialList;
import com.ngnconstruction.Model.request.MaterialListRequest;
import com.ngnconstruction.Model.response.MaterialListResponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;
import com.ngnconstruction.paginate.Paginate;
import com.ngnconstruction.recycler.LoadingListItemCreator;
import com.ngnconstruction.recycler.LoadingListItemSpanLookup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialListFragment extends Fragment implements Paginate.Callbacks {


    private static final String TAG = MaterialListFragment.class.getSimpleName();
    ApiInterfaceListener listener;

    MaterialListAdapter adapter;
    int totalPageFromApi;
    List<MaterialList> data;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerMaterials)
    RecyclerView recyclerMaterials;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.txtNoData)
    TextView txtNoData;
    Unbinder unbinder;
    private boolean loading = false;
    private int page;
    protected long networkDelay = 2000;
    protected int totalPages;
    private Handler handler;
    protected int threshold = 8;
    private Paginate paginate;
    protected boolean customLoadingListItem = false;
    private static final int GRID_SPAN = 3;
    protected boolean addLoadingRow = true;
    LinearLayoutManager llm;


    private BroadcastReceiver mBroadcastReceiver;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_material_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        initRecycliver();
        handler = new Handler();
        data = new ArrayList<>();

        listener = APIClient.getClient().create(ApiInterfaceListener.class);

        if (Network.isConnected(Objects.requireNonNull(getActivity()))) {
            loadList();
        } else {
            Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
        }
        initreceiver();
        setRegisterListener();
        return view;
    }

    private void setRegisterListener() {
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Network.isConnected(Objects.requireNonNull(getActivity()))) {
                    loadList();
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initRecycliver() {
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerMaterials.setHasFixedSize(true);
        recyclerMaterials.setNestedScrollingEnabled(false);
        recyclerMaterials.setItemAnimator(new DefaultItemAnimator());
        recyclerMaterials.setLayoutManager(llm);
    }


    private void initreceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                adapter.clear();
                loadList();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBroadcastReceiver, new IntentFilter(IntentConstants.ACTION_MATERIAL));
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void loadList() {

        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        if (swiperefresh != null) {
            swiperefresh.setRefreshing(true);
        }

        Call<MaterialListResponse> call = listener.listMaterial(user_id, api_token, "1");
        call.enqueue(new Callback<MaterialListResponse>() {
            @Override
            public void onResponse(Call<MaterialListResponse> call, Response<MaterialListResponse> response) {
                if (response.isSuccessful()) {
                    MaterialListResponse request = response.body();
                    if (request != null) {
                        MaterialListRequest listData = request.getData();
                        totalPageFromApi = listData.getLastPage();
                        page = 1;
                        hasLoadedAllItems();
                        data = listData.getData();
                        if (data.size() > 0) {
                            setupPagination(data);
                        } else {
                            recyclerMaterials.setVisibility(View.GONE);
                            txtNoData.setVisibility(View.VISIBLE);
                        }

                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (swiperefresh != null) {
                    swiperefresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<MaterialListResponse> call, Throwable t) {
                t.printStackTrace();
                if (swiperefresh != null) {
                    swiperefresh.setRefreshing(false);
                }
            }
        });
    }

    private void setpaginateData(int page) {
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Call<MaterialListResponse> call = listener.listMaterial(user_id, api_token, String.valueOf(page));
        call.enqueue(new Callback<MaterialListResponse>() {
            @Override
            public void onResponse(Call<MaterialListResponse> call, Response<MaterialListResponse> response) {
                MaterialListResponse request = response.body();
                if (request != null) {
                    MaterialListRequest listData = request.getData();
                    data = listData.getData();

                    if (data.size() > 0) {
                        adapter.add(data);
                        loading = false;
                    } else {
                        loading = true;
                    }
                }
            }

            @Override
            public void onFailure(Call<MaterialListResponse> call, Throwable t) {
            }
        });
    }


    protected void setupPagination(List<MaterialList> list) {
        if (paginate != null) {
            paginate.unbind();
        }
        handler.removeCallbacks(fakeCallback);
        adapter = new MaterialListAdapter(list);
        loading = false;
        if (recyclerMaterials != null) {
            recyclerMaterials.setAdapter(adapter);
            Log.e("totalPageFromApi", String.valueOf(totalPageFromApi));
            if (totalPageFromApi > 1) {

                paginate = Paginate.with(recyclerMaterials, this)
                        .setLoadingTriggerThreshold(threshold)
                        .addLoadingListItem(addLoadingRow)
                        .setLoadingListItemCreator(customLoadingListItem ? new CustomLoadingListItemCreator() : null)
                        .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                            @Override
                            public int getSpanSize() {
                                return GRID_SPAN;
                            }
                        })
                        .build();
            }
        }

    }

    @Override
    public synchronized void onLoadMore() {
        Log.d("Paginate", "onLoadMore");
        loading = true;
        handler.postDelayed(fakeCallback, networkDelay);
    }

    @Override
    public synchronized boolean isLoading() {
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems() {
        Log.e("page", String.valueOf(page));
        totalPages = totalPageFromApi;
        return page == totalPages;
    }

    private Runnable fakeCallback = new Runnable() {
        @Override
        public void run() {
            {
                page++;

                if (page < totalPages + 1) {
                    Log.e("page", String.valueOf(page));
                    if (Network.isConnected(getActivity())) {
                        setpaginateData(page);
                    } else {
                        Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    loading = true;
                }
            }
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.custom_loading_list_item, parent, false);
            return new ActiveJobFragment.VH(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            VH vh = (VH) holder;
            vh.tvLoading.setText(String.format("Total items loaded: %d.\nLoading more...", adapter.getItemCount()));
            if (recyclerMaterials.getLayoutManager() instanceof StaggeredGridLayoutManager) {
                StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) vh.itemView.getLayoutParams();
                params.setFullSpan(true);
            }
        }
    }

    static class VH extends RecyclerView.ViewHolder {
        TextView tvLoading;

        public VH(View itemView) {
            super(itemView);
            tvLoading = itemView.findViewById(R.id.tv_loading_text);
        }
    }

}
