package com.ngnconstruction.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.JobListData;
import com.ngnconstruction.Model.request.JobListRequest;
import com.ngnconstruction.Model.response.JobListRersponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.EventDecorator;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.activity.JobDetailActivity;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeeklyCalenderFragment extends Fragment {


    private static final String TAG = MonthlyCalenderFragment.class.getSimpleName();
    Unbinder unbinder;
    @BindView(R.id.calendarView)
    MaterialCalendarView calendarView;
    private ProgressDialog mProgressDialog;
    private ApiInterfaceListener listener;
    private ArrayList<CalendarDay> dates;

    public WeeklyCalenderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = null;
        try {
            view = inflater.inflate(R.layout.fragment_weekly_calender, container, false);
            unbinder = ButterKnife.bind(this, view);
            listener = APIClient.getClient().create(ApiInterfaceListener.class);
            initDialog();
            LocalDate today = calendarView.getCurrentDate().getDate();

            String start_date = formatDate(today.withDayOfMonth(1));
            String end_date = formatDate(today.withDayOfMonth(today.lengthOfMonth()));
            if (Network.isConnected(getActivity())) {
                loadList(start_date, end_date);
            } else {
                Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }
    private void initDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void loadList(String start_date, String end_date) {
        mProgressDialog.show();

        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Log.e(TAG, String.format(Locale.getDefault(), "Start : %s,End : %s", start_date, end_date));

        retrofit2.Call<JobListRersponse> call = listener.doGetJobCalendar(user_id, api_token, start_date, end_date,"1");
        call.enqueue(new Callback<JobListRersponse>() {
            @Override
            public void onResponse(retrofit2.Call<JobListRersponse> call, Response<JobListRersponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    JobListRersponse request = response.body();


                    if (request != null) {
                        JobListData listData = request.getData();

                        setDates(listData.getData());
                    }

                } else {
                    try {

                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(retrofit2.Call<JobListRersponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

    private void setDates(List<JobListRequest> data) {
        for (int i = 0; i < data.size(); i++) {
            Calendar calendar = Calendar.getInstance();

            String dt = data.get(i).getCreatedAt();

            SimpleDateFormat mainformat = new SimpleDateFormat("yyyy-M-dd", Locale.getDefault());

            try {
                Date dateFrom = mainformat.parse(dt);
                calendar.setTime(dateFrom);
//                calendarView.setDateSelected(CalendarDay.from(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)), true);
                calendarView.addDecorator(new EventDecorator(getResources().getColor(R.color.bg_color),CalendarDay.from(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH))));

                getDates(LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)));
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initCalender();
    }

    private void initCalender() {
        calendarView.setAllowClickDaysOutsideCurrentMonth(false);
        calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay calendarDay, boolean b) {
                LocalDate today =calendarDay.getDate();

                String start_date = formatDate(today);
                String end_date = formatDate(today);

                if (Network.isConnected(Objects.requireNonNull(getActivity()))) {
                    openJobDetailActivity(start_date, end_date);
                } else {
                    Toast.makeText(getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {

                if (Network.isConnected(Objects.requireNonNull(WeeklyCalenderFragment.this.getActivity()))) {
                    LocalDate today = materialCalendarView.getCurrentDate().getDate();
                    String start_date = WeeklyCalenderFragment.this.formatDate(today.withDayOfMonth(1));
                    String end_date = WeeklyCalenderFragment.this.formatDate(today.withDayOfMonth(today.lengthOfMonth()));
                    WeeklyCalenderFragment.this.loadList(start_date, end_date);
                } else {
                    Toast.makeText(WeeklyCalenderFragment.this.getActivity(), "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private String formatDate(LocalDate localDate) {
        return localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
    private void openJobDetailActivity(String start_date, String end_date) {
        Intent intent = new Intent(getActivity(), JobDetailActivity.class);
        intent.putExtra(IntentConstants.START_DATE, start_date);
        intent.putExtra(IntentConstants.END_DATE, end_date);
        startActivity(intent);
    }

    private void getDates(LocalDate temp) {
        dates = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            final CalendarDay day = CalendarDay.from(temp);
            dates.add(day);
            temp = temp.plusDays(5);
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
