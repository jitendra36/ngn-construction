package com.ngnconstruction.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ngnconstruction.Adapter.JobDetailListAdapter;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.JobListData;
import com.ngnconstruction.Model.request.JobListRequest;
import com.ngnconstruction.Model.response.JobListRersponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.MessageUtil;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobDetailActivity extends AppCompatActivity {

    private static final String TAG = JobDetailActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_home)
    RecyclerView recyclerView;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.txtNoJobs)
    TextView txtNoJobs;
    private ProgressDialog mProgressDialog;
    private List<JobListRequest> data;
    private ApiInterfaceListener listener;
    private JobDetailListAdapter adapter;
    private String start_date, end_date;
    private BroadcastReceiver mBroadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        ButterKnife.bind(this);
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        data = new ArrayList<>();
        initActionBar();
        initDialog();
        intiRecycview();
        Intent intent = getIntent();
        if (intent.hasExtra(IntentConstants.START_DATE)) {
            start_date = intent.getStringExtra(IntentConstants.START_DATE);
        }
        if (intent.hasExtra(IntentConstants.END_DATE)) {
            end_date = intent.getStringExtra(IntentConstants.END_DATE);
        }
        if (Network.isConnected(this)) {
            loadList(start_date, end_date);
        } else {
            MessageUtil.showToast(this, "Kindly connect to internet & try again.");
        }
        initReceiver();
        setRegisterListener();
    }
    private void initReceiver() {
        mBroadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                loadList(start_date, end_date);
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver,new IntentFilter(IntentConstants.ACTION_JOB_LIST_UPDATE));
    }
    private void setRegisterListener() {
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (Network.isConnected(JobDetailActivity.this)) {
                    JobDetailActivity.this.loadList(start_date, end_date);
                } else {
                    MessageUtil.showToast(JobDetailActivity.this, "Kindly connect to internet & try again.");
                }
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initActionBar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("JOB LIST");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void intiRecycview() {
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(llm);
        adapter = new JobDetailListAdapter(this, data);
        recyclerView.setAdapter(adapter);
    }

    private void loadList(String start_date, String end_date) {
        mProgressDialog.show();

        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");

        Log.e(TAG, String.format(Locale.getDefault(), "Start : %s,End : %s", start_date, end_date));

        Call<JobListRersponse> call = listener.doGetJobCalendar(user_id, api_token, start_date, end_date, "1");
        call.enqueue(new Callback<JobListRersponse>() {
            @Override
            public void onResponse(Call<JobListRersponse> call, Response<JobListRersponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    JobListRersponse request = response.body();


                    if (request != null) {
                        JobListData listData = request.getData();
                        if (listData.getData().size() > 0) {
                            swiperefresh.setVisibility(View.VISIBLE);
                            txtNoJobs.setVisibility(View.GONE);
                            setDates(listData.getData());
                        } else {
                            swiperefresh.setVisibility(View.GONE);
                            txtNoJobs.setVisibility(View.VISIBLE);
                        }

                    } else {
                        swiperefresh.setVisibility(View.GONE);
                        txtNoJobs.setVisibility(View.VISIBLE);
                    }

                } else {
                    try {

                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<JobListRersponse> call, Throwable t) {
                mProgressDialog.dismiss();
                swiperefresh.setVisibility(View.GONE);
                txtNoJobs.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setDates(List<JobListRequest> data) {
        this.data.addAll(data);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);

    }
}
