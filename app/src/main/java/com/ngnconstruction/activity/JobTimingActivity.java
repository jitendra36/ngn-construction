package com.ngnconstruction.activity;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.ngnconstruction.Adapter.JobTimingAdapter;
import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.response.JobTimingResponse;
import com.ngnconstruction.Model.response.SimpleResponse;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.MessageUtil;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobTimingActivity extends AppCompatActivity {

    private static final String TAG = JobTimingActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerJobTimings)
    RecyclerView recyclerJobTimings;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.txtNoJobs)
    TextView txtNoJobs;
    @BindView(R.id.txtStatusInOut)
    TextView txtStatusInOut;
    @BindView(R.id.txtClockInOut)
    TextView txtClockInOut;
    @BindView(R.id.txtJobTitle)
    TextView txtJobTitle;
    @BindView(R.id.txtJobTotalHour)
    TextView txtJobTotalHour;
    @BindView(R.id.txtTime)
    TextView txtTime;
    private ApiInterfaceListener listener;
    private ArrayList<JobTimingResponse.Hoursdata> data;
    private ProgressDialog mProgressDialog;
    private JobTimingAdapter mAdapter;
    private int job_id = -1;
    private String jobName = "", time = "", status = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_timing);
        ButterKnife.bind(this);
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        data = new ArrayList<>();
        initActionBar();
        initDialog();
        intiRecycview();
        Intent intent = getIntent();
        if (intent.hasExtra(IntentConstants.JOB_ID)) {
            job_id = intent.getIntExtra(IntentConstants.JOB_ID, -1);
        }
        if (intent.hasExtra(IntentConstants.JOB_IN_OUT_STATUS)) {
            status = String.valueOf(intent.getIntExtra(IntentConstants.JOB_IN_OUT_STATUS, -1));
        }
        if (intent.hasExtra(IntentConstants.JOB_NAME)) {
            jobName = intent.getStringExtra(IntentConstants.JOB_NAME);
        }
        if (Network.isConnected(this)) {
            getJobTimings(job_id);
        } else {
            MessageUtil.showToast(this, "Kindly connect to internet & try again.");
        }
        txtJobTitle.setText(jobName);
        if (status.equalsIgnoreCase(String.valueOf(0))) {
            txtStatusInOut.setText("STATUS");
            txtClockInOut.setText("CLOCK IN");
            txtClockInOut.setTag(getResources().getString(R.string.tagClockIn));
        } else {
            txtClockInOut.setText("CLOCK OUT");
            txtStatusInOut.setText("STATUS");
            txtClockInOut.setTag(getResources().getString(R.string.tagClockOut));
        }
        setRegisterListener();
    }

    private void setRegisterListener() {
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Network.isConnected(JobTimingActivity.this)) {
                    mAdapter.clear();
                    getJobTimings(job_id);
                } else {
                    MessageUtil.showToast(JobTimingActivity.this, "Kindly connect to internet & try again.");
                }
            }
        });
    }

    private void getJobTimings(int job_id) {
        swiperefresh.setRefreshing(true);

        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");


        Call<JobTimingResponse> call = listener.doGetJobTimings(Integer.parseInt(user_id), api_token, job_id);
        call.enqueue(new Callback<JobTimingResponse>() {
            @Override
            public void onResponse(Call<JobTimingResponse> call, Response<JobTimingResponse> response) {

                if (response.isSuccessful()) {
                    JobTimingResponse request = response.body();
                    switch (request.getCode()) {
                        case "200":

                            JobTimingResponse.Data listData = request.getData();
                            if (listData.getHoursdata().size() > 0) {
                                swiperefresh.setVisibility(View.VISIBLE);
                                txtNoJobs.setVisibility(View.GONE);
                                data.addAll(listData.getHoursdata());
                                mAdapter.notifyDataSetChanged();

                            } else {
                                swiperefresh.setVisibility(View.GONE);
                                txtNoJobs.setVisibility(View.VISIBLE);
                            }

                            txtJobTotalHour.setText(String.format(Locale.getDefault(), "%sHrs", listData.getTotal_hours()));
                            break;
                        default:
                            swiperefresh.setVisibility(View.GONE);
                            txtNoJobs.setVisibility(View.VISIBLE);
                            break;
                    }


                } else {
                    try {

                        Log.e("onResponse: ", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                swiperefresh.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<JobTimingResponse> call, Throwable t) {
                swiperefresh.setRefreshing(false);
                swiperefresh.setVisibility(View.GONE);
                txtNoJobs.setVisibility(View.VISIBLE);
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initActionBar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDefaultDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("JOB TIMINGS");
        }

    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void intiRecycview() {
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerJobTimings.setItemAnimator(new DefaultItemAnimator());
        recyclerJobTimings.setLayoutManager(llm);
        mAdapter = new JobTimingAdapter(data);
        recyclerJobTimings.setAdapter(mAdapter);
    }

    private void showDateTimePicker(Context context) {
        final Calendar currentDate = Calendar.getInstance();
        final Calendar date = Calendar.getInstance();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        final SimpleDateFormat formatShow = new SimpleDateFormat("yyyy/MM/dd HH:mm a", Locale.getDefault());
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                date.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        date.set(Calendar.MINUTE, minute);
                        time = (format.format(date.getTime()));
                        txtTime.setText(formatShow.format(date.getTime()));
                        if (!time.equalsIgnoreCase("")) {
                            if (txtClockInOut.getTag().toString().equalsIgnoreCase(getResources().getString(R.string.tagClockIn))) {
                                checkoutIn("0", time, getResources().getString(R.string.tagClockIn));
                            } else {
                                checkoutIn("1", time, getResources().getString(R.string.tagClockOut));
                            }
                        } else {
                            MessageUtil.showToast(JobTimingActivity.this, "Please Select Date & Time First");

                        }
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE));
        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.show();
    }

    private void checkoutIn(String flag, String getTime, String clocktag) {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<SimpleResponse> call = listener.checkInOutJob(user_id, api_token, String.valueOf(job_id), flag, getTime);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()) {
                    SimpleResponse simpleResponse = response.body();
                    switch (simpleResponse.getCode()) {
                        case "200":

                            if (clocktag.equalsIgnoreCase(getResources().getString(R.string.tagClockIn))) {
                                txtStatusInOut.setText("STATUS IN");
                                txtClockInOut.setText("CLOCK OUT");
                                txtClockInOut.setTag(getResources().getString(R.string.tagClockOut));
                            } else {
                                txtClockInOut.setTag(getResources().getString(R.string.tagClockIn));
                                txtClockInOut.setText("CLOCK IN");
                                txtStatusInOut.setText("STATUS OUT");
                            }
                            MessageUtil.showToast(JobTimingActivity.this, simpleResponse.getMessage());
                            time = "";
                            mAdapter.clear();
                            LocalBroadcastManager.getInstance(JobTimingActivity.this).sendBroadcast(new Intent(IntentConstants.ACTION_JOB_LIST_UPDATE));
                            getJobTimings(job_id);
                            break;
                        default:
                            MessageUtil.showToast(JobTimingActivity.this, simpleResponse.getMessage());
                            break;
                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.txtStatusInOut, R.id.txtClockInOut})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.txtClockInOut:
                showDateTimePicker(JobTimingActivity.this);
                break;
        }
    }

}
