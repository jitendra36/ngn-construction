package com.ngnconstruction.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ngnconstruction.AppConstants.AppConstants;
import com.ngnconstruction.Model.request.MaterialdetailRequest;
import com.ngnconstruction.Model.response.MaterialDetailResponse;
import com.ngnconstruction.Model.response.StockUpdateResponce;
import com.ngnconstruction.R;
import com.ngnconstruction.Utility.IntentConstants;
import com.ngnconstruction.Utility.Network;
import com.ngnconstruction.Utility.Pref;
import com.ngnconstruction.network.APIClient;
import com.ngnconstruction.network.ApiInterfaceListener;

import java.io.IOException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialDetailActivity extends AppCompatActivity {

    private static final String TAG = MaterialDetailActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtMaterial)
    TextView txtMaterial;
    @BindView(R.id.txtQuantity)
    TextView txtQuantity;
    @BindView(R.id.layoutDetail)
    LinearLayout layoutDetail;
    @BindView(R.id.edtQuantity)
    TextInputEditText edtQuantity;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    String id;
    int totalStock;
    ApiInterfaceListener listener;
    protected ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        initDialog();
        initActionBar();
        listener = APIClient.getClient().create(ApiInterfaceListener.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            id = i.getStringExtra(AppConstants.RequestDataKey.id);
            String title = i.getStringExtra("title");
            getSupportActionBar().setTitle(title);

            if (Network.isConnected(MaterialDetailActivity.this)) {
                loadDetail(id);
            } else {
                Toast.makeText(MaterialDetailActivity.this, "Kindly connect to internet & try again.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setMessage("Please wait..");
    }

    @SuppressLint("RestrictedApi")
    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDefaultDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        String stock = edtQuantity.getText().toString().trim();
        if (!stock.equals("")) {
            int addStock = Integer.parseInt(stock);
            if (totalStock >= addStock) {
                updateStack(id, String.valueOf(addStock));
            } else {
                Toast.makeText(MaterialDetailActivity.this, "Enter less than total stock", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(MaterialDetailActivity.this, "Please Enter stock", Toast.LENGTH_SHORT).show();

        }

    }

    private void updateStack(String mt_id, String stc) {
        edtQuantity.setText("");
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<StockUpdateResponce> call = listener.getMaterialUsed(user_id, api_token, mt_id, stc, "");
        call.enqueue(new Callback<StockUpdateResponce>() {
            @Override
            public void onResponse(Call<StockUpdateResponce> call, Response<StockUpdateResponce> response) {

                if (response.isSuccessful()) {

                    StockUpdateResponce detailResponse = response.body();
                    if (detailResponse != null) {
                        switch (detailResponse.getCode()) {
                            case 200:
                                if (detailResponse.getData() != null) {
                                    totalStock = detailResponse.getData();
                                    txtQuantity.setText(String.format(Locale.getDefault(), "%d Qty.", detailResponse.getData()==null?0:detailResponse.getData()));
                                }
                                Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                LocalBroadcastManager.getInstance(MaterialDetailActivity.this).sendBroadcast(new Intent(IntentConstants.ACTION_MATERIAL));
                                finish();
                                loadDetail(id);
                                break;
                            default:
                                Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                break;
                        }

                    } else {
                        Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                mProgressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<StockUpdateResponce> call, Throwable t) {
                mProgressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void loadDetail(String id) {
        mProgressDialog.show();
        String user_id = Pref.getValue(AppConstants.preDataKey.user_id, "");
        String api_token = Pref.getValue(AppConstants.preDataKey.api_token, "");
        Call<MaterialDetailResponse> call = listener.materialDetail(user_id, api_token, id);
        call.enqueue(new Callback<MaterialDetailResponse>() {
            @Override
            public void onResponse(Call<MaterialDetailResponse> call, Response<MaterialDetailResponse> response) {

                if (response.isSuccessful()) {
                    MaterialDetailResponse detailResponse = response.body();
                    if (detailResponse != null) {
                        MaterialdetailRequest request = detailResponse.getData();

                        txtMaterial.setText(request.getMaterialName());

                        if (request.getStock() != null) {
                            totalStock = request.getStock();
                            txtQuantity.setText(String.format(Locale.getDefault(), "%d Qty.", request.getStock() == null ? 0 : request.getStock()));
                        }

//                    Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(MaterialDetailActivity.this, detailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    try {
                        Log.e(TAG, "onResponse: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                mProgressDialog.dismiss();


            }

            @Override
            public void onFailure(Call<MaterialDetailResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }
}
